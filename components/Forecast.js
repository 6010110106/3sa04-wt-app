import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class Forecast extends Component {
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.textBig}>{this.props.main}</Text>
				<Text style={styles.texts}>{this.props.description}</Text>
				<View style={styles.tempDisplay}>
				<Text style={styles.textBig}>{this.props.temp}</Text>
				<Text style={styles.texts}> °C</Text>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		textAlign: 'center',
		alignItems: 'center',
		justifyContent: 'space-evenly',
		flexDirection: 'column'
	},
	tempDisplay: {
		textAlign: 'center',
		textAlignVertical: 'top',
		flexDirection: 'row',
		alignItems: 'flex-start',
		justifyContent: 'center'
	},
	textBig: {
		marginVertical: 8,
		color: '#fffc',
		fontSize: 48,
	},
	texts: {
		marginVertical: 8,
		color: '#fffc',
		fontSize: 20,
	}
})
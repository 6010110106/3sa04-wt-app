import React, { Component } from 'react'
import { Text, View, StyleSheet, Button, 
	 FlatList, TouchableHighlight } from 'react-native'

const availableZips = [ 
	{ place: 'Hatyai',     code: '90110' },
	{ place: 'Trang',      code: '92000' },
	{ place: 'Chiang Mai', code: '50000' },
	{ place: 'Khon Kaen',  code: '40000' },
	{ place: 'Chonburi',   code: '20000' },
	{ place: 'Yala',       code: '95000' }
]

const ZipItem = ({place, code, navigate}) => (
	<TouchableHighlight onPress={() => navigate('Weather', {zipCode: code})}>
		<View style={styles.zipItem}>
			<Text style={styles.zipPlace}>{place}</Text>
			<Text style={styles.zipCode}>{code}</Text>
		</View>
	</TouchableHighlight>
)

const _keyExtractor = item => item.code

export default class ZipCodeScreen extends Component {
	static navigationOptions = ({navigation}) => {
		return {
			headerTitle: (<Text>Select Zip Code</Text>),
		}
	}

	render() {
		const { navigate } = this.props.navigation
		return (
			<View>
				<FlatList 
					data={availableZips}
					keyExtractor={_keyExtractor}
					renderItem={({item}) => 
						<ZipItem {...item} navigate={navigate} />
					}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	zipItem: {
		flexDirection: 'row',
		backgroundColor: '#0001'
	},
	zipPlace: {
		flex: 2,
		margin: 8
	},
	zipCode: {
		textAlign: 'right',
		flex: 1,
		margin: 8
	}
})
import React, { Component } from 'react'
import { Text, View, Button, StyleSheet, 
	TouchableHighlight } from 'react-native'
import Weather from './Weather'

export default class WeatherScreen extends Component {
	static navigationOptions = ({navigation}) => {
		return {
			headerTitle: (<Text style={styles.header}>Weather</Text>),
			headerRight: (
				<Button 
					style={styles.changeZip}
					title='Change'
					onPress={() => navigation.navigate('ZipCode')}
				/>
			)
		}
	}

	render() {
		const zipCode = this.props.navigation.getParam('zipCode')
		return (
			<Weather zipCode={zipCode} />
		)
	}
}

const styles = StyleSheet.create({
	changeZip: {
		padding: 8,
		margin: 8,
		backgroundColor: '#fff0'
	},
	header: {
		margin: 8
	}
})
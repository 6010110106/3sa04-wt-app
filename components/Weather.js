import React, { Component } from 'react'
import { StyleSheet, Text, View , ImageBackground} from 'react-native'
import Forecast from './Forecast';

//const openWeatherAppID = 'fd68c0f2039c5a25f666a9ff374bc93e'
const openWeatherAppID = 'dff441b0becbebca8f5f883201cac9c8'
const openWeatherUrl = (zipCode) => {
	return (`http://api.openweathermap.org/data/2.5/`
	+ `weather?q=${zipCode},th&units=metric&APPID=${openWeatherAppID}`)
}

export default class Weather extends Component {
	constructor(props) {
		super(props)
		this.state = {
			forecast: {
				name: '...', main: '...', description: '...', temp: 0
			}
		}
	}
	fetchForecast = () => {
		fetch(openWeatherUrl(this.props.zipCode))
		.then((response) => response.json())
		.then((json) => {
			this.setState({
				forecast: {
					name: json.name,
					main: json.weather[0].main,
					description: json.weather[0].description,
					temp: json.main.temp
				}
			})
		})
		.catch((error) => {
			console.warn(error)
		})
	}
	componentDidMount = () => this.fetchForecast()
	componentDidUpdate = (prevs) => {
		if (prevs.zipCode !== this.zipCode) {
			this.fetchForecast()
		}
	}
	
	render() {
		return (
			<ImageBackground source={require('../assets/1558371943399.jpg')}
			style={styles.backdrop}>
			<View style={styles.container}>
			<View style={styles.zipLine}>
			<Text style={styles.zipText}>Zip code: </Text>
			<Text style={styles.zipDisplay}>
				{this.props.zipCode} ({this.state.forecast.name})
			</Text>
			</View>
			<Forecast {...this.state.forecast} />
			</View>
			
			</ImageBackground>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		height: '40%',
		width: '100%',
		paddingTop: 25,
		backgroundColor: '#0007',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	zipLine: {
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row'
	},
	zipText: {
		color: '#fff',
		fontSize: 20
	},
	zipDisplay: {
		fontFamily: 'monospace',
		color: '#fff',
		fontSize: 20
	},
	dummy: {
		flex: 1
	},
	backdrop: {
		width: '100%',
		height: '100%',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	}
})
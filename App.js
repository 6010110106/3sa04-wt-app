import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'

import ZipCodeScreen from './components/ZipCodeScreen'
import WeatherScreen from './components/WeatherScreen';

const RootStack = createStackNavigator({
	Weather: WeatherScreen,
	ZipCode: ZipCodeScreen
},{
	initialRouteName: 'Weather',
	initialRouteParams: { zipCode: '95000' }
})

const AppContainer = createAppContainer(RootStack)

export default class App extends React.Component {
	render () {
		return (
			<AppContainer />
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
